import torch
import torch.nn as nn
import torch.optim as optim

class MLP(nn.Module):
    '''
    Multi-layer perceptron.
    '''
    def __init__(self, n_in, n_out, hidden_sizes, activation):
        '''
        Initialize an MLP neural network.
        Parameters
        ----------
        n_in: int
            Input dimension.
        n_out: int
            Output dimension.
        hidden_sizes: list
            List of sizes of hidden layers.
        activation: str
            Type of activation function, [relu, tanh, softmax] are supported.
        '''
        super().__init__()
        
        self.fc = nn.ModuleList()
        last_size = n_in
        for size in hidden_sizes:
            self.fc.append(nn.Linear(last_size, size))
            last_size = size
        self.fc.append(nn.Linear(last_size, n_out))

        ac_map = {
            'relu': torch.relu, # range [0, #]
            'tanh': torch.tanh, # range [-1, 1]
            'softmax': torch.softmax, # range [0, 1]
        }
        assert activation in ac_map, f"activation type {activation} doesn't supported"
        self.ac = ac_map[activation]

    def forward(self, x):
        for fc in self.fc[:-1]:
            x = self.ac(fc(x))
        x = self.fc[-1](x)
        return x

    def basis_func(self, x):
        for fc in self.fc[:-1]:
            x = self.ac(fc(x))
        return x


class NeuralNetwork():
    '''
    Simple neural network
    '''
    def __init__(self, n_var = 10, hidden_size = 50, hidden_layers = 8, activation = 'softmax', lr = 1e-2, weight_decay = 1e-4, n_epoch = 100, **kwargs):
        '''
        Parameters
        ----------
        n_var: int
            Size of the 1D input
        hidden_size: int
            Size of the hidden layer of the neural network.
        hidden_layers: int
            Number of hidden layers of the neural network.
        activation: str
            Type of activation function.
        lr: float
            Learning rate.
        weight_decay: float
            Weight decay.
        n_epoch: int
            Number of training epochs.
        '''
        super().__init__()

        self.net = MLP(n_in = n_var, n_out = 1, hidden_sizes = (hidden_size,) * hidden_layers, activation = activation)
        self.criterion = nn.MSELoss()
        self.optimizer = optim.Adam(self.net.parameters(), lr = lr, weight_decay = weight_decay)
        self.n_epoch = n_epoch

    def _fit(self, X, Y):
        X, Y = torch.FloatTensor(X), torch.FloatTensor(Y) # X: Input; Y: GT of output
        for _ in range(self.n_epoch):
            Y_pred = self.net(X)[:, 0]
            loss = self.criterion(Y_pred, Y)
            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step()
