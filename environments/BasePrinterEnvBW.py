import gym
from gym import spaces
import numpy as np
import cv2

def encoder(color):
    return color

def decoder(pressure):
    return pressure

class NozzleBW():
    def __init__(self, init_pressure, mixing_ratio):
        self.pressure = init_pressure
        self.mixingRatio = mixing_ratio

    def step(self, new_pressure) :
        self.pressure = self.pressure + self.mixingRatio *(new_pressure - self.pressure)
        return self.pressure


class PrinterEnvBW (gym.Env) :
    def __init__(self, img, sight = 5):
        super(PrinterEnvBW, self).__init__()
        self.sight = sight
        self.observation_shape = self.sight*2+1
        self.action_space = spaces.Box(low=0.0, high=1.0, shape=(1,), dtype=np.float32)
        self.observation_space = spaces.Box(low=0, high=1.0, shape=(self.observation_shape, ), dtype=np.float32)
        self.targets = self.load_targets(img)# targets should be the whole image with any sizes, rather than a windows
        self.index = self.sight
        self.nozzle = NozzleBW(0.0,0.0)
        self.obs = np.zeros((self.observation_shape, 2))
        self.canvas = np.zeros(self.observation_shape)

    def load_targets(self, img):
        return img.flatten()

    def draw_canvas(self, color):
        self.canvas[self.index] = color

    def _get_obs(self):
        self.obs[:, 0] = self.canvas
        self.obs[:, 1] = self.targets[self.index-self.sight:self.index+self.sight+1]

        return (self.obs * 255).astype(np.uint8)
        #return self.targets[self.index-self.sight:self.index+self.sight+1]

        
    def _get_info(self):
        return dict()

    def step(self, action):
        action = np.clip(action, 0., 1.)
        
        # Assert that it is a valid action 
        assert self.action_space.contains(action), "Invalid Action"

        outColor = decoder(self.nozzle.step(action))
        self.draw_canvas(outColor)

        reward = - np.abs(self.targets[self.index] - outColor)
        reward = float(reward[0])
        done = self.index + 5 == len(self.targets)
        observation = self._get_obs()
        info = self._get_info()

        self.index += 1
        return observation, reward, done, info

    def reset(self, targets, initColor = 0):
        initValue = np.ones(self.sight) * initColor
        endValue = np.ones(self.sight) * targets[len(targets)-1]

        self.index =  self.sight
        self.targets = np.concatenate((initValue,targets,endValue))

        self.canvas = np.zeros(self.observation_shape)

        self.nozzle = NozzleBW(encoder(initColor), 0.2)
        return self._get_obs()

    def render(self, mode = "human"):
        assert mode in ["human", "rgb_array"], "Invalid mode, must be either \"human\" or \"rgb_array\""
        if mode == "human":
            cv2.imshow("Printing", self.canvas)
            cv2.waitKey(10)
        
        elif mode == "rgb_array":
            return self.canvas
    
    def close(self):
        cv2.destroyAllWindows()