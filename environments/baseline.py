import gym
from gym import spaces
import math
import numpy as np
import random
import scipy
import importlib
import sys
import cv2

input_size = 128*128 # flatten a 128*128 2D image to 1D array
windows_size = 10

class Nozzle(object):
    def __init__(self, pos_max):
        self.pos = 0
        self.pos_min = 0
        self.pos_max = pos_max
        self.pressure = 0
    
    def set_position(self, pos):
        self.pos = self.clamp(pos, self.pos_min, self.pos_max)
    
    def set_pressure(self, value):
        self.pressure = value

    def get_pressure(self):
        return self.pressure
    
    def get_position(self):
        return self.pos
    
    def move(self):
        self.pos += 1
        self.pos = self.clamp(self.pos, self.pos_min, self.pos_max)

    # get the color from the pressure. this version is a simple linear mapping relation
    def get_color(self):
        return self.pressure

    def reset(self):
        self.pos = 0
        self.pressure = 0

    def clamp(self, n, minn, maxn):
        return max(min(maxn, n), minn)

class BasePrinterEnv(gym.Env):# black-white printer with color randomly sampled from [0, 1]
    """Custom Environment that follows gym interface"""
    metadata = {'render.modes': ['human', 'rgb_array']}

    def __init__(self):
        super(BasePrinterEnv, self).__init__()
        # Define a 1-D observation space
        self.observation_shape = (input_size,)
        self.observation_space = spaces.Box(low = np.zeros(self.observation_shape), 
                                            high = np.ones(self.observation_shape),
                                            dtype = np.float32)

        # observation for training    
        self.obs = np.zeros((input_size, 2))

        # Define an action space ranging from 0 to 1
        self.action_space = spaces.Box(low=0.0, high=1.0, shape=(1,), dtype=np.float32)

        # 
                        
        # Create a canvas to render the environment images upon 
        self.canvas = np.zeros(self.observation_shape)

        # Create a nozzle to move, change the pressure, and print color
        self.Nozzle = Nozzle(input_size)

        # 
        self.target = 0

    def draw_canvas(self):
        # print the color on the current location
        pos = self.Nozzle.get_position()
        color = self.Nozzle.get_color()
        self.canvas[pos] = color
        
        # then go to next location
        self.Nozzle.move()

    def get_observation(self):
        self.obs[:, 0] = self.canvas
        self.obs[:, 1] = self.target

        return (self.obs * 255).astype(np.uint8)

    def reset(self):

        # Reset the Canvas 
        self.canvas = np.zeros(self.observation_shape)

        # Reset the Nozzle
        self.Nozzle.reset()

        # return the observation
        return self.canvas 
    
    def render(self, mode = "human"):
        assert mode in ["human", "rgb_array"], "Invalid mode, must be either \"human\" or \"rgb_array\""
        if mode == "human":
            cv2.imshow("Printing", self.canvas)
            cv2.waitKey(10)
        
        elif mode == "rgb_array":
            return self.canvas
    
    def close(self):
        cv2.destroyAllWindows()

    def cal_reward(self): # ToDo
        return 0

    def step(self, action):
        # Flag that marks the termination of an episode
        done = False

        action = np.clip(action, 0., 1.)
        
        # Assert that it is a valid action 
        assert self.action_space.contains(action), "Invalid Action"
        
        # Reward for executing a step.
        reward = self.cal_reward()    

        # apply the action to the nozzle
        self.Nozzle.set_pressure(action)

        # draw on the canvas
        self.draw_canvas()

        # If reach the edge, end the episode
        pos = self.Nozzle.get_position()
        if pos == input_size:
            done = True

        return self.get_observation(), reward, done, []

            