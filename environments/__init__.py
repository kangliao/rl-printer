from gym.envs.registration import register

register(
    id = 'BasePrinterEnv-black',
    entry_point = 'environments.baseline:BasePrinterEnv',
    max_episode_steps = 1000
)