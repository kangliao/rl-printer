import gym
from gym import spaces
import numpy as np

def encoder(color) :
    return color

def decoder(pressure) :
    return pressure

class NozzleBW():
    def __init__(self, init_pressure, mixing_ratio):
        self.pressure = init_pressure
        self.mixingRatio = mixing_ratio

    def step(self, new_pressure) :
        self.pressure = self.pressure + self.mixingRatio *(new_pressure - self.pressure)
        return self.pressure


class PrinterEnvBW (gym.Env) :
    def __init__(self, sight: int = 5,):
        super(PrinterEnvBW, self).__init__()
        self.sight = sight
        self.action_space = spaces.Box(low=0.0, high=1.0, shape=(1,), dtype=np.float32)
        self.observation_space = spaces.Box(low=0, high=1.0, shape=(self.sight*2+1, ), dtype=np.float32)
        self.targets = np.zeros(self.sight*2+1)
        self.index = self.sight
        self.nozzle = NozzleBW(0.0,0.0)
        
    def _get_obs(self):
        return self.targets[self.index-self.sight:self.index+self.sight+1]

        
    def _get_info(self):
        return dict()

    def step(self, action):
        outColor = decoder(self.nozzle.step(action))

        reward = - np.abs(self.targets[self.index] - outColor)
        reward = float(reward[0])
        done = self.index + 5 == len(self.targets)
        observation = self._get_obs()
        info = self._get_info()

        self.index = self.index +1
        return observation, reward, done, info


    def reset(self, targets= [0], initColor = 0):
        initValue = np.ones(self.sight) * initColor
        endValue = np.ones(self.sight) * targets[len(targets)-1]

        self.index =  self.sight
        self.targets = np.concatenate((initValue,targets,endValue))

        self.nozzle = NozzleBW(encoder(initColor), 0.2)
        return self._get_obs()
